module.exports = Dezip;

var unzip = require('unzip');
var request = require('request');
var wf = new WF();

function Dezip(dataConf)
{
	this.code = function(req, res)
	{
		this.dezip(req, res);
	}

	this.dezip = function(req, res)
	{
		if (req.url == '/dezip') {
			req.continue = false;

			this.getGame(req, (gameData) => {
				this.getAccount(gameData.author_id, (accountData) => {
					// Create folder
					this.createFolder(gameData, accountData, (folder) => {
						// Clone and write
						this.cloneAndWrite(gameData, folder, function () {
							// Dezip
							UTILS.httpUtil.dataSuccess(req, res, 'Response to dezip', {
							}, "0.1" );
						});
					});
				});
			});
		}
	}

	this.getGame = function(req, callback) {
		request('http://127.0.0.1/api/0.1/get/game/id:' + req.post.id + ';', function (error, response, body) {
			if (!error && response.statusCode == 200) {
				body = JSON.parse(body);
				callback(body.data[0]);
			}
		});
	}

	this.getAccount = function(authorId, callback) {
		// request('http://127.0.0.1/api/0.1/get/user/id:' + authorId, function (error, response, body) {
		// 	if (!error && response.statusCode == 200) {
		// 		body = JSON.parse(body);
		// 		callback(body.data);
		// 	}
		// });

		callback({"pseudo":"laygen"});
	}

	this.createFolder = function(gameData, accountData, callback) {
		folderName = this.generateFolderName(gameData, accountData);

		var path =  wf.CONF['SRV_PATH'] + "web/host/default/zone/" + folderName;

		var folder = {
			name: folderName,
			path: path,
		};

		// Mkdir
		fs.mkdir(path, function (err) {
			callback(folder);
		});
	}

	this.cloneAndWrite = function(gameData, folder, callback) {
		var clonePath = wf.CONF['SRV_PATH'] + "web/host/default/plugin/dezip/clone";
		fs.mkdir(folder.path + "/page", function () {
			fs.mkdir(folder.path + "/page/game", function () {
				fs.mkdir(folder.path + "/page/game/view", function () {
					fs.readFile(clonePath + "/zone.conf.js", 'utf8', function (err, content) {
						var re = new RegExp("_URI","g");
				    content = content.replace(re, folder.name);
						fs.writeFile(folder.path + "/" + folder.name + ".conf.js", content, function (err) {
							fs.readFile(clonePath + "/page/game/game.conf.js", 'utf8', function (err, content) {
								fs.writeFile(folder.path + "/page/game/game.conf.js", content, function (err) {
									fs.readFile(clonePath + "/page/game/game.page.js", 'utf8', function (err, content) {
										fs.writeFile(folder.path + "/page/game/game.page.js", content, function (err) {
											fs.createReadStream(gameData.path_zip)
												.pipe(unzip.Parse())
												.on("entry", function (entry) {
													if (entry.path.match(".*.html")) {
														entry.pipe(fs.createWriteStream(folder.path + "/page/game/view/game.html"));
													}
													else {
														entry.autodrain();
													}
												})
												.on("close", function() {
													callback();
												});
										});
									});
								})
							});
						});
					});
				});
			});
		});
	}

	this.generateFolderName = function(gameData, accountData) {
		var nameFolder = accountData.pseudo + "_" + gameData.name;
		return nameFolder;
	}
}

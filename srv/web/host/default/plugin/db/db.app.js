module.exports = Data;

function Data(dataConf)
{
	var dbs = {};

	function loadDBS()
	{

		var db = dataConf.conf.config.database;
		// LOAD DBS
		dbs[db] = new UTILS.FDB({database: db});
	}
	loadDBS();

	this.code = function(req, res)
	{
		req.DB = dbs;
	}
}

module.exports = Upload;

var fs = require('fs');
var wf = new WF();

function Upload(dataConf)
{
	this.code = function(req, res)
	{
		this.parseFile(req, res);
	}

	this.parseFile = function(req, res) {
		if (req.url == '/file/upload') {
			var path =  wf.CONF['VAR_PATH'] + 'uploads';
			var fileName = req.post.id;
			var fullPath = path + '/' + fileName;

			var url = "http://" + req.host + "/content/var/uploads/" + fileName;
			req.continue = false;

			fs.appendFile(fullPath, req.post.data, (err) => {
				if (err) throw err;

				UTILS.httpUtil.dataSuccess(req, res, 'Response to upload', {
					path: fullPath,
					url: url,
				}, "0.1" );
			});
		}
	}
}

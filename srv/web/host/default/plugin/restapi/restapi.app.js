module.exports = RESTAPI;
var UAPI = "api";

function RESTAPI(hostConf)
{
	var V01 = new require('./lib/v01.js');
	this.code = function(req, res)
	{
		if(req.DB == undefined) return;
		var root = "/";
		var uri = req.url.replace(root, "");

		var parts = uri.split('/');
        if(parts[4])
        {
            var endParts = parts.splice(4).join("/");
            parts[4] = endParts;
        }
		if(parts[0] !== undefined && parts[0] == UAPI)
		{
            var error = function(code, msg, version)
            {
                res.end('{ "result": { "status": "error", "version": "' + version + '", "code": ' + code + ', "message": "' + msg + '" } }');
            }
            
			req.continue = false;

			  if(parts[1] !== undefined)
			  {
				switch(parts[1])
				{
				  case "0.1":
				    V01.DB = hostConf.conf.config.database;
					V01.req = req;
					V01.res = res;
					V01.HOST = req.HOST;
					V01.parse(parts);
					break;
				  default:
					error(2, "undefined api version", "null");
					break;
				}
			  }
			else
			  error(1, "undefined api version", "null");
		}
        
        
	}

                /*
                  error :
                  0 : no error
                  1 : undefined version
                  2 : undefined selector
				  21 : unknown selector
                  3 : selector empty
                  4 : post empty

                100: bad http method

                */
}

module.exports = restfulPage;

function restfulPage(page)
{
    this.code = function(req, res)
    {
		var rest = this.view.restful;
		var result = "";
		var method = ["GET", "POST", "PUT", "DELETE"];
		var access = "Access";
		var doc = "Doc";
		var screen = "Screen";
		var link = "Link";
		var desc = "Description";

		var QUERY = "query";
		var DATA = "data";

		var title = "";
			if(page.conf.config.title) title = page.conf.config.title;
		rest = rest.replace(/_TITLE/gi, title);

		for(var m in req.HOST.MODELS)
		{
			var objModel = new req.HOST.MODELS[m]();
			if(objModel.rest)
			{
				var tmpBloc = this.view.bloc.slice(0);
				var name = "";

				tmpBloc = tmpBloc.replace(/_MODEL/gi, m);

				var mdesc = "";
				if(objModel.description) mdesc = objModel.description;
				tmpBloc = tmpBloc.replace(/_DESCRIPTION/gi, mdesc);

				var maccess = "";
				if(objModel.access) maccess = page.conf.config.access;
				tmpBloc = tmpBloc.replace(/_ACCESS/gi, maccess);

				var methodResult = "";
				for(var i in method)
				{
					if(objModel[method[i]] == undefined || objModel[method[i]] == true)
					{
						var tmpMethod = this.view.method;

						var parameter = "";
						var example_result = {};
						tmpMethod = tmpMethod.replace(/_MODEL/gi, m);
						tmpMethod = tmpMethod.replace(/_METHOD_LOWER/gi, method[i].toLowerCase());
						tmpMethod = tmpMethod.replace(/_METHOD_UPPER/gi, method[i].toUpperCase());
						tmpMethod = tmpMethod.replace(/_FUNCTION/gi, method[i] + "(this, '" + m + "', '" + page.conf.config.url + "')");

						// method description
						var gdesc = "";
						if(objModel[method[i].toLowerCase() + desc]) gdesc = objModel[method[i].toLowerCase() + desc];
							tmpMethod = tmpMethod.replace(/_DESCRIPTION/gi, gdesc);

						// method access
						var gaccess = "";
						if(objModel[method[i].toLowerCase() + access]) gaccess = page.conf.config.access;
						tmpMethod = tmpMethod.replace(/_ACCESS/gi, gaccess);


						// DOC
						var tmpdoc = "";
						if(objModel[method[i].toLowerCase() + doc])
						{
							tmpdoc = this.view.doc;
							tmpdoc = tmpdoc.replace(/_DOC/gi, objModel[method[i].toLowerCase() + doc]);
						}
						tmpMethod = tmpMethod.replace(/_DOC/gi, tmpdoc);

						// SCREEN
						var tmpscreen = "";
						if(objModel[method[i].toLowerCase() + screen])
						{
							tmpscreen = this.view.screen;
							tmpscreen = tmpscreen.replace(/_SCREEN/gi, objModel[method[i].toLowerCase() + screen]);
						}
						tmpMethod = tmpMethod.replace(/_SCREEN/gi, tmpscreen);

						// LINK
						var tmplink = "";
						if(objModel[method[i].toLowerCase() + link])
						{
							tmplink = this.view.link;
							tmplink = tmplink.replace(/_LINK/gi, objModel[method[i].toLowerCase() + link]);
						}
						tmpMethod = tmpMethod.replace(/_LINK/gi, tmplink);

						for(var f in objModel.field)
						{
							example_result[f] = objModel.field[f].example || "";
							var tmpParameter = this.view.parameter;
							tmpParameter = tmpParameter.replace(/_VAR/gi, QUERY);
							tmpParameter = tmpParameter.replace(/_NAME/gi, f);

							// check require :
							var require = "";
							if(objModel.field[f].require) require = "*";
							tmpParameter = tmpParameter.replace(/_REQUIRE/gi, require);

							// check description :
							var odesc = "";
							if(objModel.field[f].description) odesc = objModel.field[f].description;
							tmpParameter = tmpParameter.replace(/_DESCRIPTION/gi, odesc);

							// check type :
							var otype = "";
							if(objModel.field[f].type) otype = objModel.field[f].type;
							tmpParameter = tmpParameter.replace(/_TYPE/gi, otype);

							// check default value :
							var bydefault = "";
							if(objModel.field[f].bydefault) bydefault = objModel.field[f].bydefault;
							tmpParameter = tmpParameter.replace(/_DEFAULT_VALUE/gi, bydefault);

							parameter += tmpParameter;
						}
						tmpMethod = tmpMethod.replace(/_PARAMETER/gi, parameter);
						tmpMethod = tmpMethod.replace(/_EXAMPLE_RESULT/gi, JSON.stringify(example_result));

						var data = "";

						// SET DATA PARAMETERS
						if(method[i] == "PUT")
						{
							data = this.view.data;
							var dataParam = "";
							for(var f in objModel.field)
							{
								var tmpParameter = this.view.parameter;
								tmpParameter = tmpParameter.replace(/_VAR/gi, DATA);
								tmpParameter = tmpParameter.replace(/_NAME/gi, f);

								// check require :
								var require = "";
								if(objModel.field[f].require) require = "*";
								tmpParameter = tmpParameter.replace(/_REQUIRE/gi, require);

								// check description :
								var odesc = "";
								if(objModel.field[f].description) odesc = objModel.field[f].description;
								tmpParameter = tmpParameter.replace(/_DESCRIPTION/gi, odesc);

								// check type :
								var otype = "";
								if(objModel.field[f].type) otype = objModel.field[f].type;
								tmpParameter = tmpParameter.replace(/_TYPE/gi, otype);

								// check default value :
								var bydefault = "";
								if(objModel.field[f].bydefault) bydefault = objModel.field[f].bydefault;
								tmpParameter = tmpParameter.replace(/_DEFAULT_VALUE/gi, bydefault);

								dataParam += tmpParameter;
							}
							data = data.replace(/_PARAMETER/gi, dataParam);
							data = data.replace(/_DATA/gi, method[i]);
						}

						tmpMethod = tmpMethod.replace(/_DATA/gi, data);
						methodResult += tmpMethod;
					}
				}

				tmpBloc = tmpBloc.replace(/_METHOD/gi, methodResult);
				result += tmpBloc;
			}
		}
		rest = rest.replace("_VIEW", result);
        res.end(rest);
    };
}

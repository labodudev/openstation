module.exports = LogoutPage;

function LogoutPage(ok)
{
    this.code = function(req, res)
    {
        res.end(this.view.logout);
    };
}

module.exports = gamePage;

var request = require('request');

function gamePage(ok)
{
    this.code = function(req, res)
    {
      var view = this.view;
      request('http://127.0.0.1/api/0.1/get/game/' + req.param.name, function (error, response, body) {
        if (!error && response.statusCode == 200) {
          body = JSON.parse(body);
          var data = body.data;

          var template = view.game;
          template = parseTemplate(data[0], template);
          template = parseFor(data[0]['tags'], 'tags', template);
          if (data[0]['lang']) {
            template = parseFor(data[0]['lang']['interface'], 'lang-interface', template);
            template = parseFor(data[0]['lang']['audio'], 'lang-audio', template);
            template = parseFor(data[0]['lang']['subtitle'], 'lang-subtitle', template);
          }
          res.end(template);
        }
      })
    };
}

function parseTemplate(data, template) {
  for (var key in data) {
    var re = new RegExp("_" + key.toUpperCase(),"g");
    template = template.replace(re, decodeURIComponent(data[key]));
  }

  return template;
}

function parseFor(data, name, template) {
  var content = "";
  if(data) {
    for (var i = 0; i < data.length; i++) {
      content += "<li>" + data[i] + "</li>";
    }

    var re = new RegExp("(_FOR class='" + name + "'.*>)(.*)(<)");
    template = template.replace(re, "$1" + content + "$3");
  }

  return template;
}

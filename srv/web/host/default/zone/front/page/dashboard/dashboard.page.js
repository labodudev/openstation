module.exports = dashboardPage;

var wf = new WF();
function dashboardPage(ok)
{
    this.code = function(req, res)
    {
      if (req.cookie.connected) {
        res.end(this.view.dashboard);
      }

      if(req.HOST.ZONES.front.PAGES.login.uri)
      {
        wf.Redirect(res, req.HOST.ZONES.front.PAGES.login.uri);
      }
    };
}

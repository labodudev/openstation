"use strict"

/**
* @namespace form
* @author Jimmy Latour <lelabodudev@gmail.com>
*/

define(['./xhr'], function (xhr, form) {
  return {
    /**
    * @memberof form
    * @since 0.0.1
    * @description Make $_GET data for XHR.
    * @author Jimmy Latour <lelabodudev@gmail.com>
    * @param {Object} data to transform to $_GET string.
    * @return {string} data for XHR.
    */
    parseGet: function (data) {
      var url = "";
      for (var key in data) {
        if (data[key]) {
          url += encodeURIComponent(key) + ":" + encodeURIComponent(data[key]) + ";";
        }
      }

      return url;
    },
    /**
    * @memberof form
    * @since 0.0.1
    * @description Parse to query
    * @author Jimmy Latour <lelabodudev@gmail.com>
    * @param {Object} data to add to query object.
    * @return {string} data for XHR.
    */
    parsePost: function (data) {
      var query = {
        query: data
      };

      return JSON.stringify(query);
    },
    /**
    * @memberof form
    * @since 0.0.1
    * @description Check if name contains []
    * @author Jimmy Latour <lelabodudev@gmail.com>
    * @param {string} name The name to checked
    * @return {boolean}
    */
    isArray: function (name) {
      var re = new RegExp(/\[.*\]/);
      if (name.match(re)) {
        return true;
      }

      return false;
    },
    /**
    * @memberof form
    * @since 0.0.1
    * @description Check if name contains [a-z]
    * @author Jimmy Latour <lelabodudev@gmail.com>
    * @param {string} name The name to be checked
    * @return {boolean}
    */
    haveSubObject: function(name) {
      var re = new RegExp(/\[([a-z]+)\]/);

      var match = name.match(re);

      if(match) {
        return true;
      }

      return false;
    },
    /**
    * @memberof form
    * @since 0.0.1
    * @description Parse array content to an Object.
    * @author Jimmy Latour <lelabodudev@gmail.com>
    * @param {Object} object The name to be checked
    * @todo Refactorisation
    * @return {Object} The parsed object
    */
    parseArray: function(object, name, data) {
      var re = new RegExp(/\[([a-z]+)\]/);

      var match = data.name.match(re);
      if (match && match[1]) {
        if(!object[match[1]]) {
          object[match[1]] = new Array();
        }
        object[match[1]].push(encodeURIComponent(data.value));
      }
      else {
        object[name] = encodeURIComponent(data.value);
      }

      return object;
    },
    /**
    * @memberof form
    * @since 0.0.1
    * @description Parse array content to an Object.
    * @author Jimmy Latour <lelabodudev@gmail.com>
    * @param {Object} object The name to be checked
    * @todo Refactorisation
    * @return {Object} The parsed object
    */
    parseFormToObject: function(formData) {
      var object = {};
      for ( var i = 0; i < formData.length; i++) {
        if (formData[i].name && !formData[i].getAttribute('_NO_FORM')) {
          if (this.isArray(formData[i].name)) {
            var re = new RegExp(/([a-z]+)\[/);
            var name = formData[i].name.match(re)[1];

            if (haveSubObject(formData[i].name)) {
              if (!object[name]) {
                object[name] = {};
              }
              parseArray(object[name], name, formData[i]);

            }
            else {
              if (!object[name]) {
                object[name] = new Array();
              }
              object[name].push(encodeURIComponent(formData[i].value));
            }

          }
          else {
            object[formData[i].name] = encodeURIComponent(formData[i].value);
          }
        }
      }

      return object;
    }
  };
});

"use strict"

/**
* @namespace cookie
* @author Jimmy Latour <lelabodudev@gmail.com>
*/

define([], function (cookie) {
  return {
    /**
    * @memberof cookie
    * @since 0.0.1
    * @description Create a cookie
    * @author Jimmy Latour <lelabodudev@gmail.com>
    * @param {string} name The cookie name
    * @param {any} value The cookie value
    * @return {void}
    */
    setCookie: function (name, value) {
      var today = new Date(), expires = new Date();
      expires.setTime(today.getTime() + (365*24*60*60*1000));
      document.cookie = encodeURIComponent(name) + "=" + encodeURIComponent(value) + ";expires=" + expires.toGMTString();
    },
    /**
    * @memberof cookie
    * @since 0.0.1
    * @description Get a cookie
    * @author Jimmy Latour <lelabodudev@gmail.com>
    * @param {string} name The cookie name
    * @return {any} The cookie
    */
    getCookie: function (name) {
      var cookContent = document.cookie, cookEnd, i, j, c;
      var name = name + "=";

      for (i=0, c=cookContent.length; i<c; i++) {
        j = i + name.length;
        if (cookContent.substring(i, j) == name) {
          cookEnd = cookContent.indexOf(";", j);
          if (cookEnd == -1) {
            cookEnd = cookContent.length;
          }
          return decodeURIComponent(cookContent.substring(j, cookEnd));
        }
      }
      return null;
    },
    /**
    * @memberof cookie
    * @since 0.0.1
    * @description Delete a cookie
    * @author Jimmy Latour <lelabodudev@gmail.com>
    * @param {string} name The cookie name
    * @return {void}
    */
    deleteCookie: function (name) {
      if (this.getCookie(name)) {
        document.cookie = name + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
      }
    }

  };
});

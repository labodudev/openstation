"use strict"

/**
* @namespace HTMLSelectElement
* @author Jimmy Latour <lelabodudev@gmail.com>
*/


define(['../template', '../chosen'], function (template, chosen) {
  /**
  * @memberof HTMLSelectElement
  * @since 0.0.1
  * @description Make multiple select like jquery.chosen.
  * This method declare call parseOption, declareInput and selectLi.
  * @author Jimmy Latour <lelabodudev@gmail.com>
  * @return {void}
  */
  HTMLSelectElement.prototype.chosen = function() {
    var element = this;
    template.getTemplate('/assets/js/lib/view/chosen.view.html', function(template) {
      template = chosen.parseOption(template, element);

      var newElement = document.createElement('div');
      newElement.className = "chosen-container";
      newElement.innerHTML = template;
      element.parentNode.appendChild(newElement);
      var name = element.getAttribute('name');
      element.remove();

      chosen.declareInput(newElement);
      chosen.selectLi(newElement, name);
    });
  }

  /**
  * @memberof NodeList
  * @since 0.0.1
  * @description Browse the NodeList and call chosen method.
  * @author Jimmy Latour <lelabodudev@gmail.com>
  * @return {void}
  */
  NodeList.prototype.chosen = function() {
    for(var i = this.length - 1; i >= 0; i--) {
      if(this[i]) {
        this[i].chosen();
      }
    }
  }
});

"use strict"

define(function (require) {
  var xhr = require('lib/xhr'),
      template = require('lib/template');
      require('lib/parser/auth.parser');

  function getGames(callback) {
    var xhrRequest = xhr.getXMLHttpRequest();

    xhrRequest.onreadystatechange = function() {
      if (xhrRequest.readyState == 4 && (xhrRequest.status == 200 || xhrRequest.status == 0)) {
        callback(JSON.parse(xhrRequest.responseText));
      }
    };

    xhrRequest.open("GET", "/api/0.1/get/game/validate:true", true);
    xhrRequest.send(null);
  }

  function displayGames(games) {
    template.getTemplate('assets/js/view/game/item.view.html', function(content) {
      for (var key in games) {
        var element = document.createElement('li');
        element.innerHTML = template.parseTemplate(content, games[key]);
        document.querySelector('ul.games').appendChild(element);
      }
    });
  }

  getGames(function (response) {
    displayGames(response.data);
  });
});

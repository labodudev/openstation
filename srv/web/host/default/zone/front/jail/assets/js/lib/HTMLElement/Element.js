/**
* @namespace Element
* @author Jimmy Latour <lelabodudev@gmail.com>
*/

define(['../child'], function (child) {
  /**
  * @memberof Element
  * @since 0.0.1
  * @description Remove the current element.
  * @author Jimmy Latour <lelabodudev@gmail.com>
  * @return {void}
  */
  Element.prototype.remove = function() {
    this.parentElement.removeChild(this);
  }

  /**
  * @namespace NodeList
  * @author Jimmy Latour <lelabodudev@gmail.com>
  */

  /**
  * @memberof NodeList
  * @since 0.0.1
  * @description Remove all element in the NodeList
  * @author Jimmy Latour <lelabodudev@gmail.com>
  * @return {void}
  */
  NodeList.prototype.remove = HTMLCollection.prototype.remove = function() {
    for(var i = this.length - 1; i >= 0; i--) {
      if(this[i] && this[i].parentElement) {
        this[i].parentElement.removeChild(this[i]);
      }
    }
  }

  /**
  * @memberof Element
  * @since 0.0.1
  * @description Trigger event
  * @author Jimmy Latour <lelabodudev@gmail.com>
  * @param {string} name The name of event
  * @return {void}
  */
  Element.prototype.triggerEvent = function(name) {
    var event; // The custom event that will be created

    if (document.createEvent) {
      event = document.createEvent("HTMLEvents");
      event.initEvent(name, true, true);
    } else {
      event = document.createEventObject();
      event.eventType = name;
    }

    event.eventName = name;

    if (document.createEvent) {
      this.dispatchEvent(event);
    } else {
      this.fireEvent("on" + event.eventType, event);
    }
  }

  /**
  * @memberof Element
  * @since 0.0.1
  * @description Call find function
  * @author Jimmy Latour <lelabodudev@gmail.com>
  * @return {any}
  */
  Element.prototype.find = function(name) {
    return child.find(this, undefined, name);
  }
});

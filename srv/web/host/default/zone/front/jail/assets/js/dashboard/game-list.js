"use strict"

define(function (require) {
  var xhr = require('../lib/xhr'),
      template = require('../lib/template');

  function getGames(callback) {
    var xhrRequest = xhr.getXMLHttpRequest();

    xhrRequest.onreadystatechange = function() {
      if (xhrRequest.readyState == 4 && (xhrRequest.status == 200 || xhrRequest.status == 0)) {
        callback(JSON.parse(xhrRequest.responseText));
      }
    };

    xhrRequest.open("GET", "/api/0.1/get/game/validate:false", true);
    xhrRequest.send(null);
  }

  function displayGames(games) {
    template.getTemplate('../assets/js/dashboard/view/game/item.view.html', function(content) {
      for (var key in games) {
        var element = document.createElement('li');
        element.innerHTML = template.parseTemplate(content, games[key]);
        document.querySelector('ul.games').appendChild(element);

      }

      addEventToValidate();
    });
  }

  function addEventToValidate() {
    var links = document.querySelectorAll('ul.games a');
    for (var i = 0; i < links.length; i++) {
      links[i].addEventListener('click', clickOnValidate);
    }
  }

  function clickOnValidate(evt) {
    evt.preventDefault();

    var gameId = this.getAttribute('data-id');

    var data = {
      "query": {
        "id": gameId,
        "validate": "false"
      },
      "update": {
        "validate": "true"
      }
    };

    var xhrRequest = xhr.getXMLHttpRequest();

    xhrRequest.onreadystatechange = function() {
      if (xhrRequest.readyState == 4 && (xhrRequest.status == 200 || xhrRequest.status == 0)) {
        dezipGame(gameId);
      }
    };

    xhrRequest.open("PUT", '/api/0.1/put/game', true);
    xhrRequest.send(JSON.stringify(data));
  }

  getGames(function (response) {
    displayGames(response.data);
  });

  function dezipGame (gameId) {
    var xhrRequest = xhr.getXMLHttpRequest();

    var data = {
      "id": gameId,
    };

    // Envoie de la requête pour dezip
    xhrRequest.open("POST", "/dezip", true);
    xhrRequest.send(JSON.stringify(data));
  }

});

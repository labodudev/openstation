"use strict"

/**
* @namespace child
* @author Jimmy Latour <lelabodudev@gmail.com>
*/

define([], function (child) {
  return {
    /**
    * @memberof child
    * @since 0.0.1
    * @description Found element by className.
    * Found element children by className.
    * @author Jimmy Latour <lelabodudev@gmail.com>
    * @return {any}
    */
    find: function (element, foundedElement, name) {
      var re = new RegExp(name);

      if (element.className.match(re)) {
        foundedElement = element;
      }

      if (element.children) {
        for (var i = 0; i < element.children.length; i++) {
          foundedElement = this.find(element.children[i], foundedElement, name);
        }
      }

      return foundedElement;
    }
  };
});

"use strict"

/**
* @namespace template
* @author Jimmy Latour <lelabodudev@gmail.com>
*/

define(['./xhr'], function (xhr, template) {
  return {
    /**
    * @memberof template
    * @since 0.0.1
    * @description Use XHR for call template
    * @author Jimmy Latour <lelabodudev@gmail.com>
    * @param {string} url The template url
    * @param {string} callback The callback function
    * @return {string} Use callback to return the template content
    */
    getTemplate: function (url, callback) {
      var xhrRequest = xhr.getXMLHttpRequest();

      xhrRequest.onreadystatechange = function() {
        if (xhrRequest.readyState == 4 && (xhrRequest.status == 200 || xhrRequest.status == 0)) {
          callback(xhrRequest.responseText);
        }
      };

      xhrRequest.open("GET", url, true);
      xhrRequest.send(null);
    },
    /**
    * @memberof template
    * @since 0.0.1
    * @description Loop data and replace _KEY to data
    * @author Jimmy Latour <lelabodudev@gmail.com>
    * @param {string} template The template content to parse
    * @param {object} data The data to affect in the template
    * @return {string} The parsed template
    */
    parseTemplate: function (template, data) {
      for (var key in data) {
        var re = new RegExp("_" + key.toUpperCase(),"g");
        template = template.replace(re, decodeURIComponent(data[key]));
      }

      return template;
    },
    /**
    * @memberof template
    * @since 0.0.1
    * @description Loop data for create the content.
    * Parse all _FOR key in the template _FOR for found HTMLElement, and append the content in.
    * @author Jimmy Latour <lelabodudev@gmail.com>
    * @param {string} template The template content to parse
    * @param {object} data The data to affect in the template
    * @return {string} The parsed template
    */
    parseFor: function (template, data) {
      var content = "";
      for (var i = 0; i < data.length; i++) {
        content += "<li data-value='" + data[i].value + "'>" + data[i].label + "</li>";
      }

      var re = new RegExp("(_FOR.*>)(.*)(<)");
      template = template.replace(re, "$1" + content + "$3");

      return template;
    }
  };
});

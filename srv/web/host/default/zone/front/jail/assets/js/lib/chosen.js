"use strict"

/**
* @namespace chosen
* @author Jimmy Latour <lelabodudev@gmail.com>
*/

define(['./template', './HTMLElement/Element'], function (template, chosen) {
  return {
    /**
    * @memberof chosen
    * @since 0.0.1
    * @description Call parseFor.
    * @author Jimmy Latour <lelabodudev@gmail.com>
    * @return {Object} The value of for
    */
    parseOption: function (data, element) {
      return template.parseFor(data, element);
    },
    /**
    * @memberof chosen
    * @since 0.0.1
    * @description On keyup filter the list of option by title.
    * @author Jimmy Latour <lelabodudev@gmail.com>
    * @param {DivElement} element Parent element contains children text and chosen-drop element.
    * @return {void}
    */
    declareInput: function (element) {
      var input = element.find('search-input');
      var list = element.find('chosen-drop')['children'];

      input.addEventListener('keyup', function(evt) {
        var re = new RegExp(this.value, 'i');
        for (var i = 0; i < list.length; i++) {
          if (!list[i].innerHTML.match(re)) {
            list[i].style = "display: none";
          }
          else {
            list[i].style = "display: list-item";
          }
        }
      });
    },
    /**
    * @memberof chosen
    * @since 0.0.1
    * @description Select li on click on it. Call createLi, createInput,
    * resetInput and remove it.
    * @author Jimmy Latour <lelabodudev@gmail.com>
    * @param {DivElement} element The parent element contains chosen-drop element
    * @param {string} name of chosen
    * @return {void}
    */
    selectLi: function (element, name) {
      var list = element.find('chosen-drop')['children'];
      var _this = this;
      for (var i = 0; i < list.length; i++) {
        list[i].addEventListener('click', function(evt) {
          _this.createLi(element, this);
          _this.createInput(element, this, name);
          _this.resetInput(element);
          this.remove();
        });
      }
    },
    /**
    * @memberof chosen
    * @since 0.0.1
    * @description Get containers, create li element, add class search-choice and text into it.
    * Insert it before the first element in the chosen.
    * @author Jimmy Latour <lelabodudev@gmail.com>
    * @param {DivElement} element The parent element contains chosen-drop element
    * @param {ListElement} li
    * @return {void}
    */
    createLi: function (element, li) {
      var chosenChoice = element.find('chosen-choices');
      var element = document.createElement('li');
      element.className = 'search-choice';
      element.innerHTML = "<span>" + li.innerHTML + "</span>";
      element.innerHTML += "<a href='#'>x</a>";
      chosenChoice.insertBefore(element, chosenChoice.childNodes[0]);
    },
    /**
    * @memberof chosen
    * @since 0.0.1
    * @description Create hidden input for the form.
    * @author Jimmy Latour <lelabodudev@gmail.com>
    * @param {DivElement} element The parent element contains chosen-drop element
    * @param {ListElement} li
    * @param {string} The chosen name
    * @return {void}
    */
    createInput: function (element, li, name) {
      var form = element.closest('form');
      var element = document.createElement('input');
      element.setAttribute('name', name);
      element.setAttribute('type', 'hidden');
      element.setAttribute('value', li.getAttribute('data-value'));
      form.appendChild(element);
    },
    /**
    * @memberof chosen
    * @since 0.0.1
    * @description Reset input search for reset filter.
    * @author Jimmy Latour <lelabodudev@gmail.com>
    * @param {DivElement} element The parent element contains chosen-drop element
    * @return {void}
    */
    resetInput: function (element) {
      var input = element.find('search-input');
      input.value = "";
      input.triggerEvent("keyup");
    }
  };
});

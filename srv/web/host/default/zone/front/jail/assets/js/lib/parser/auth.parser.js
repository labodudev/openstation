"use strict"

define(['./../cookie', './../HTMLElement/Element'], function (cookie) {
  var session = cookie.getCookie('connected');

  var links = document.querySelectorAll('[_NO_AUTH]'), i;
  for (i = 0; i < links.length; i++) {
    if(session) {
      links[i].remove();
    }
  }
});

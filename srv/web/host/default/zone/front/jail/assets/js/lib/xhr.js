"use strict"

/**
* @namespace xhr
* @author Jimmy Latour <lelabodudev@gmail.com>
*/

define([], function (xhr) {
  return {
    /**
    * @memberof xhr
    * @since 0.0.1
    * @description Create the XHR Object
    * @author Jimmy Latour <lelabodudev@gmail.com>
    * @return {ActiveXObject|XMLHttpRequest} The XHR Object
    */
    getXMLHttpRequest: function () {
      var xhr = null;
      if (window.XMLHttpRequest || window.ActiveXObject) {
        if (window.ActiveXObject) {
          try {
            xhr = new ActiveXObject("Msxml2.XMLHTTP");
          } catch(e) {
            xhr = new ActiveXObject("Microsoft.XMLHTTP");
          }
        } else {
          xhr = new XMLHttpRequest();
        }
      } else {
        alert("Votre navigateur ne supporte pas l'objet XMLHTTPRequest...");
        return null;
      }
      return xhr;
    }
  };
});

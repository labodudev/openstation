"use strict"

/**
* @namespace HTMLInputElement
* @author Jimmy Latour <lelabodudev@gmail.com>
*/


define(['./../SmartUpload'], function (SmartUpload) {
  /**
  * @memberof HTMLInputElement
  * @since 0.0.1
  * @description Add an upload fonction on HTMLInputElement. When element trigger change event
  * call the sendFile function.
  * @author Jimmy Latour <lelabodudev@gmail.com>
  * @param {string} url The template url
  * @param {string} callback The callback function
  * @return {string} Use callback to return the path of uploaded file.
  */
  HTMLInputElement.prototype.upload = function(url, callback) {
    var smartUp = new SmartUpload(this, url);
    smartUp.done = callback;
    smartUp.smartSize = 1024 * 6000;
  }
});

define(['./../form', './../xhr'], function (form, xhr) {
    /**
    * @memberof form
    * @since 0.0.1
    * @description Parse $_POST and $_GET data in the form into an object.
    * Send this object with XHR request.
    * @author Jimmy Latour <lelabodudev@gmail.com>
    * @param {string} url The template url
    * @param {string} method Is $_GET or $_POST
    * @param {string} callback The callback function
    * @return {string} Use callback to return the template content
    */
    HTMLFormElement.prototype.ajaxForm = function (url, method, callback) {
      var data = form.parseFormToObject(this);
      var xhrRequest = xhr.getXMLHttpRequest();

      xhrRequest.onreadystatechange = function() {
        if (xhrRequest.readyState == 4 && (xhrRequest.status == 200 || xhrRequest.status == 0)) {
          callback(JSON.parse(xhrRequest.responseText));
        }
      };

      if (method === 'GET') {
        url += form.parseGet(data);
      }

      xhrRequest.open(method, url, true);
      xhrRequest.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

      xhrRequest.send(form.parsePost(data));
    }

});

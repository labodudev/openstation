"use strict"

define(function (require) {
  var form = require('lib/form'),
      cookie = require('lib/cookie');
  require('lib/HTMLElement/HTMLFormElement');

  var elementForm = document.querySelector('form');
  var elementButton = document.querySelector("input[type='submit']");

  elementButton.addEventListener('click', function (evt) {
    evt.preventDefault();
    elementForm.ajaxForm("/api/0.1/get/login/", "GET", function (response) {
      if (response.data[0]) {
        cookie.setCookie('connected', response.data[0].id);
      }
    });
  });
});

"use strict"

define(function (require) {
  var xhr = require('../lib/xhr'),
      form = require('../lib/form'),
      chosen = require('../lib/child'),
      chosen = require('../lib/chosen');

  require('../lib/HTMLElement/HTMLFormElement');
  require('../lib/HTMLElement/HTMLSelectElement');
  require('../lib/HTMLElement/HTMLInputElement');

  var elementForm = document.querySelector('form');

  var elementButton = document.querySelector("input[type='submit']");

  elementButton.addEventListener('click', function (evt) {
    evt.preventDefault();

    var name = document.querySelector('input[name="name"]').value;
    document.querySelector('input[name="url"]').value = "http://127.0.0.1/" + name;

    elementForm.ajaxForm("/api/0.1/post/game/", "POST", function (response) {
      console.log(response);
    });
  });

  document.querySelector('.upload-thumbnail').upload("/file/upload", function (response) {
    document.querySelector('input[name="thumbnail"]').value = response.url;
  });

  document.querySelector('.upload-zip').upload("/file/upload", function (response) {
    document.querySelector('input[name="path_zip"]').value = response.path;
  });

  document.querySelectorAll('select').chosen();
});

$( document ).ready(function() {

	$('.tabs').tabs();

	$('.root > header h2').click(function() {
		$(this).closest('div.root').toggleClass('active');
	});


	$('.method > .header .title, .method > .header .description').click(function() {
		$(this).closest('ul.method').toggleClass('active');
	});

	$('.json-view').each(function() {

		$(this).html(syntaxHighlight($(this).html()));
	})

});

function syntaxHighlight(json) {

	json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');

	return '<pre>' + json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g,
	function (match) {

		var cls = 'number';
		if (/^"/.test(match)) {
			if (/:$/.test(match)) {
				cls = 'key';
			} else {
				cls = 'string';
			}
		} else if (/true|false/.test(match)) {
			cls = 'boolean';
		} else if (/null/.test(match)) {
			cls = 'null';
		}
		return '<span class="' + cls + '">' + match + '</span>';

	}) + '</pre>';
}

function GET(element, model, url)
{
	$(element.parentElement).submit(false);

	var data = $(element.parentElement).serializeArray();
	var query = "";
	for(var el in data)
	{
		if(startsWith(data[el].name, "query.") && data[el].value)
		{
			query += data[el].name.substring(6) + ":" + data[el].value + ";";
		}
	}

	var targetUrl = url + "get/" + model + "/" + query;
	element.parentElement.parentElement.getElementsByClassName("tabs")[0].getElementsByClassName("request")[0].getElementsByClassName("json-view")[0].innerHTML = targetUrl;

	ajax(element, targetUrl, "GET", null);
}

function POST(element, model, url)
{
	$(element.parentElement).submit(false);

	var data = $(element.parentElement).serializeArray();
	var query = {};
	for(var el in data)
	{
		if(startsWith(data[el].name, "query.") && data[el].value)
		{
			query[data[el].name.substring(6)] = data[el].value;
		}
	}

	var request = JSON.stringify({query: query});
	element.parentElement.parentElement.getElementsByClassName("tabs")[0].getElementsByClassName("request")[0].getElementsByClassName("json-view")[0].innerHTML = request;

	var targetUrl = url + "post/" + model + "/";

	ajax(element, targetUrl, "POST", request);
}

function PUT(element, model, url)
{
	$(element.parentElement).submit(false);

	var data = $(element.parentElement).serializeArray();
	var query = {};
	var update = {};
	for(var el in data)
	{
		if(startsWith(data[el].name, "query.") && data[el].value)
		{
			query[data[el].name.substring(6)] = data[el].value;
		}
		else if(startsWith(data[el].name, "data.") && data[el].value)
		{
			update[data[el].name.substring(5)] = data[el].value;
		}
	}

	var request = JSON.stringify({query: query, update: update});
	element.parentElement.parentElement.getElementsByClassName("tabs")[0].getElementsByClassName("request")[0].getElementsByClassName("json-view")[0].innerHTML = request;

	var targetUrl = url + "put/" + model + "/";

	ajax(element, targetUrl, "PUT", request);
}

function DELETE(element, model, url)
{
	$(element.parentElement).submit(false);

	var data = $(element.parentElement).serializeArray();
	var query = "";
	for(var el in data)
	{
		if(startsWith(data[el].name, "query.") && data[el].value)
		{
			query += data[el].name.substring(6) + ":" + data[el].value + ";";
		}
	}

	var targetUrl = url + "delete/" + model + "/" + query;
	element.parentElement.parentElement.getElementsByClassName("tabs")[0].getElementsByClassName("request")[0].getElementsByClassName("json-view")[0].innerHTML = targetUrl;

	ajax(element, targetUrl, "DELETE", null);
}

function startsWith(haystack, needle, position)
{
      position = position || 0;
      return haystack.substr(position, needle.length) === needle;
};

function ajax(element, url, method, data)
{
	var ajaxTime = new Date().getTime();
	$.ajax({
	  url: url,
	  method: method,
	  data: data,
	   statusCode: {
		200: function(result) {
		  var responseTime = new Date().getTime()-ajaxTime;
		  responseTime += "ms";
		  element.parentElement.parentElement.getElementsByClassName("tabs")[0].getElementsByClassName("result")[0].getElementsByClassName("json-view")[0].innerHTML = result;
		  element.parentElement.parentElement.getElementsByClassName("tabs")[0].getElementsByClassName("result")[0].getElementsByClassName("response-time")[0].getElementsByClassName("time")[0].innerHTML = responseTime;
		},
		404: function() {
		  alert( "Not found api error" );
		}
	  },
	});
}

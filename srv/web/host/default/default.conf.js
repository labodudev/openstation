var hostConf=
{
	"state": true,
	"pos": 0,
	"default_zone": "front", // default zone for /
	"default_page": "home", // default view for /
  "host":
  {
    "*": "localhost",
  },

  "app":
  {
		"log": "Logger app"
  },
	"COOKIE_NAME": "cookie",
	"COOKIE_KEY": "cookie",
	"KEY": "cookie"
}
module.exports = hostConf

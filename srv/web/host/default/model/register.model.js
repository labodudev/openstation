module.exports = register;

// require nodemailer module
var nodemailer = require('nodemailer');
var REGISTRATION_URL = "http://openstation.com/register/";
var VALID_KEY_LENGTH = 15;
var KEY_FINAL = function(data){ return UTILS.Crypto.createMD5(data); };
var SMTP_PASS = "SMTP-PASSWORD";
var SMTP_ACCOUNT = "email@yourdomain.com";
var SMTP_HOST = "yourdomain.com";
var SMTP_PORT = 587;
var VERSION = "1.0";

function sendEmail(obj)
{
    var smtpConfig =
    {
        host: SMTP_HOST,
        port: SMTP_PORT,
        tls: {rejectUnauthorized: false},
        auth:
        {
            user: SMTP_ACCOUNT,
            pass: SMTP_PASS
        }
    };
    var transporter = nodemailer.createTransport(smtpConfig);

    // setup e-mail data with unicode symbols
    var mailOptions =
    {
        from: '"OpenStation Registration" <no-reply@openstation.com>', // sender address
        to: obj.to, // list of receivers
        subject: 'Yeah ! Thank you for your registration', // Subject line
        text: 'Welcome to OpenStation, to finish your registration, please follow this url : ' + REGISTRATION_URL + obj.key, // plaintext body
        html: '<b>Welcome to OpenStation, to finish your registration, please follow this url : ' + REGISTRATION_URL + obj.key  + '</b>'// html body
    };

    // send mail with defined transport object
    transporter.sendMail(mailOptions, function(error, info){
        if(error)
        {
            // error
        }
        else
        {
            // no error
        }
    });
    }

function register(empty)
{

    this.collection = "user";
    this.rest = true;
    this.POST = true;
    this.PUT = false;
    this.DELETE = false;
    this.GET = false;


    // this.verify = function(req, res, cb)
    // {
    //   console.log(req.HOST.conf)
    //     if(!req.cookie[req.HOST.conf.COOKIE_NAME])
    //     {
    //          UTILS.httpUtil.dataError(req, res, "Error", "You must allow cookies on OpenStation !", 500, VERSION)
    //     }
    //     else
    //     {
    //         var cookie = UTILS.Cookie.Get(req, req.HOST.conf.KEY, req.HOST.conf.COOKIE_NAME);
    //         if(!UTILS.Cookie.Check(req, cookie, req.HOST.conf.COOKIE_KEY, false))
    //         {
    //              UTILS.httpUtil.dataError(req, res, "Error", "You must allow cookies on OpenStation !", 500, VERSION)
    //         }
    //         else
    //         {
    //             cb()
    //         }
    //     }
    // }

    this.postSuccess = function(req, res, data, version)
    {
        var mailObj =
        {
            to: data[0].email,
            firstname: data[0].firstname,
            key: data[0].verification_key,
        }
        sendEmail(mailObj);
        UTILS.httpUtil.dataSuccess(req, res, "POST OK", data, VERSION);
    }

    this.field =
    {
        email:
        {
            require: true,
			unique: true,
            modifier: function(data)
            {
                return data.toLowerCase();
            },
			// validator: function(data)
			// {
      //           if(typeof data != "string") return false;
			// 	var validEmail = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
			// 	return validEmail.test(data);
			// },
        },
        pseudo:
        {
            modifier: function(data)
            {
                return data.toLowerCase();
            },
			validator: function(data)
			{
				// fordiden user account
                var fordiden = ["openstation", "admin", "root", "anon_"];
                if(typeof data != "string") return false;

				var validChar = "abcdefghijklmnopqrstuvwxtz0123456789-_";
                var j = data.length;
                for(var i = 0; i < j; i++)
                {
                    if(validChar.indexOf(data[i]) == -1) return false;
                }
                var n = fordiden.length;
                for(var m = 0; m < n; m++)
                {
                    if(data.indexOf(fordiden[m]) != -1) return false;
                }
				return true;
			},
        },
        firstname:
        {
            validator: function(data)
            {
                if(typeof data != 'string') return false;
                else if(data.length < 2) return false;
                else if(data.length > 100) return false;
                else return true;
            },
        },
        lastname:
        {
            validator: function(data)
            {
                if(typeof data != 'string') return false;
                else if(data.length < 2) return false;
                else if(data.length > 100) return false;
                else return true;
            },
        },
        password:
        {
            require: true,
            // validator: function(data)
            // {
            //     if(typeof data != 'string') return false;
            //     else if(data.length != 64) return false;
            //     else return true;
            // },
        },
        creation_date:
        {
            bydefault: new Date(Date.now()),
            private: true
        },
        ip:
        {
            bydefault:[],
            private: true,
        },
        verification_key:
        {
            private: true,
            bydefault:  KEY_FINAL(UTILS.Crypto.randomString(VALID_KEY_LENGTH)),
        },
        verified:
        {
            bydefault: false,
            private: true,
        }
    }
    UTILS.Model.apply(this, new Array(empty));
}

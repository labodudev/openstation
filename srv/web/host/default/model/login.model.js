module.exports = login;
var VERSION = "1.0";
function login(empty)
{

    this.collection = "user";
    this.rest = true;
    this.POST = false;
    this.PUT = false;
    this.DELETE = false;
    this.GET = true;

    this.sanitize =
    {
        _id: 'id',
        password: null,
        ip:null,
        phone:null,
        channel:null,
        verification_key: null,
    }

    this.getSuccess = function(req, res, data)
    {
        if(data.length)
        {
            // if(data[0].verified)
            // {

                var token =
                {
                    logged: true,
                    userId: data[0].id,
                    userEmail: data[0].email,
                }

                var cookie = UTILS.Cookie.Create(req, req.HOST.conf.KEY, req.HOST.conf.COOKIE_KEY, req.HOST.conf.COOKIE_NAME, token, req.HOST.conf.TIME);
                UTILS.Cookie.Set(res, cookie);

                UTILS.httpUtil.dataSuccess(req, res, "Login OK", data, VERSION);
            // }
            // else
            // {
            //     UTILS.httpUtil.dataError(req, res, "Error", "Your account has not been verified. Please check your mailbox (and your spam)", 500, VERSION)
            // }
        }
        else
        {
            UTILS.httpUtil.dataError(req, res, "Error", "Login or password incorrect", 500, VERSION)
        }
    }

    this.field =
    {
        email:
        {
            require: true,
        },
        password:
        {
            require: true,
        }
    }

    UTILS.Model.apply(this, new Array(empty));
}

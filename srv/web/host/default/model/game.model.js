module.exports = game;

function game(empty)
{
	this.collection = "game";
	this.rest = true;
	this.description = "Game Model";

	this.getDescription = "Get game object";
	this.postDescription = "Create game object";
	this.putDescription = "Update game object";
	this.deleteDescription = "Delete game object";

	this.field =
	{
		id: {

		},
    name: {
		},
		thumbnail: {
		},
		short_description: {

		},
		description: {

		},
		author_id: {

		},
		price: {

		},
		published_date: {

		},
		released_date: {

		},
		updated_date: {

		},
		status: {

		},
		version: {

		},
    tags: {

    },
    lang: {

    },
		validate: {
			bydefault: false,
			type: "bool"
		},
		path_zip: {

		}
	};

  // CALL MODEL CLASS
	UTILS.Model.apply(this, new Array(empty));

}
